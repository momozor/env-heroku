use strict;
use Test::More tests => 5;

BEGIN {
    use_ok 'Env::Heroku::Pg';
    use_ok 'Env::Heroku::Redis';
    use_ok 'Env::Heroku::Mysql';
    use_ok 'Env::Heroku::Rediscloud';
    use_ok 'Env::Heroku::Cloudinary';
}
