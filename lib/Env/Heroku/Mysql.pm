use strict;
package Env::Heroku::Mysql;
# ABSTRACT: env for heroku-cleardb-mysql

use warnings;
use URI;

# VERSION

sub import {
    my ($self) = @_;

    my $dburl = $ENV{DATABASE_URL};
    if ( $dburl and $dburl =~ s/^mysql:// ) {
        my $mysqlurl = URI->new( $dburl, 'http' );
        $ENV{MYSQLHOST} = $mysqlurl->host;
        $ENV{MYSQLPORT} = $mysqlurl->port;
        $ENV{MYSQLDATABASE} = substr $mysqlurl->path, 1;
        ($ENV{MYSQLUSER},$ENV{MYSQLPASSWORD}) = split ':', $mysqlurl->userinfo;

        $ENV{DBI_DRIVER} = 'mysql';
        $ENV{DBI_DSN}    = 'dbi:mysql:'.$ENV{MYSQLDATABASE}.'@'.$ENV{MYSQLHOST}.':'.$ENV{MYSQLPORT};
        $ENV{DBI_USER}   = $ENV{MYSQLUSER};
        $ENV{DBI_PASS}   = $ENV{MYSQLPASSWORD};
    }

    return 1;
}

1;
